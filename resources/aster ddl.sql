-- create staging tables
DROP TABLE IF EXISTS public.social_sentiment_stg_brandwatch;
create fact table public.social_sentiment_stg_brandwatch
(   queryId                 decimal(19,0) not null,
    queryName               varchar,
    resourceId              varchar,
    resourceType            varchar,
    
    tags                    varchar,
    categoryDetails         varchar,
    
    continent               varchar,
	continentCode           varchar,
	country                 varchar,
	countryCode             varchar,
	state                   varchar,
	stateCode               varchar,
	county                  varchar,
	countyCode              varchar,
	city                    varchar,
	cityCode                varchar,
	latitude                varchar,
	longitude               varchar,
    
	author                  varchar,
	authorContinent         varchar,
	authorContinentCode     varchar,
	authorCountry           varchar,
	authorCountryCode       varchar,
	authorState             varchar,
	authorStateCode         varchar,
	authorCounty            varchar,
	authorCountyCode        varchar,
	authorCity              varchar,
	authorCityCode          varchar,
	    
    fullname                varchar,
    gender                  varchar,
    avatarUrl               varchar,
    professions             varchar,
    interest                varchar,
    
    domain                  varchar,
    pageType                varchar,
    accountType             varchar,
    url                     varchar,
    mediaUrls               varchar,
    
    sentiment               varchar,
    fullText                varchar,
    language                varchar,
    wordCount               varchar,
    
    facebookAuthorId        varchar,
    facebookRole            varchar,
    facebookSubtype         varchar,
    facebookShares          integer,
    facebookLikes           integer,
    facebookComments        integer,
    
    tweet_id                decimal(19,0) not null,
    twitterAuthorId         varchar,
    twitterVerified         varchar,
    twitterRole             varchar,
    twitterFollowing        varchar,
    twitterFollowers        varchar,
    twitterRetweets         varchar,
    twitterPostCount        varchar,
    twitterReplyCount       varchar,
    
    forumViews              varchar,
    forumPosts              varchar,
    blogComments            varchar,
    status                  varchar,
    retweetOf               varchar,
    replyTo                 varchar,
    insightsHashtag         varchar,
    insightsMentioned       varchar,
    influence               varchar,
    
    averageDurationOfVisit  varchar,
    averageVisits           varchar,
    pagesPerVisit           varchar,
    monthlyVisitors         varchar,
    percentMaleVisitors     integer,
    percentFemaleVisitors   integer,
    
    engagement              varchar,
    importanceAmplification integer,
    importanceReach         integer,
    impact                  integer,
    impressions             varchar,
    mozRank                 varchar,
    outreach                varchar,
    reach                   varchar,
    starred                 varchar,
    trackedLinkClicks       integer,
    trackedLinks            varchar,

    editorialValueEUR       varchar,
    editorialValueGBP       varchar,
    editorialValueUSD       varchar,

    assignment              varchar,
    lastAssignmentDate      varchar,
    backlinks               varchar,
    checked                 varchar,
    "date"                  varchar,
    displayUrls             varchar,
    expandedUrls            varchar,
    matchPositions          varchar,
    noteIds                 varchar,
    priority                varchar,
    shortUrls               varchar,
    snippet                 varchar,

    threadId                varchar,
    threadAuthor            varchar,
    threadEntryType         varchar,
    threadURL               varchar,
    threadCreated           varchar,
    
    extracted_dt            timestamp not null
 ) distribute by hash(tweet_id);
GRANT SELECT ON public.social_sentiment_stg_brandwatch to public;

DROP TABLE IF EXISTS public.social_sentiment_stg_twitter;
create fact table public.social_sentiment_stg_twitter
(   tweet_id                decimal(19,0) not null,
    tweet_text              varchar,    --NOTE: this is what comes from Twitter API
    tweet_text_uni_esc      varchar,    --NOTE: this is unicode escaped text
    lang                    varchar,
    truncated               varchar,    --NOTE: will need to cast to bool when pushed to core
    possibly_sensitive      varchar,    --NOTE: will need to cast to bool when pushed to core
    extended_entities       varchar,
    
    favorite_count          bigint,
    retweet_count           bigint,
    
    coordinates             varchar,
    place                   varchar,
    country                 varchar,
    country_code            varchar,
    place_type              varchar,
    place_name              varchar,
    place_full_name         varchar,
    
    source                  varchar,
    contributors            varchar,
    user_id                 decimal(19,0),
    in_reply_to_screen_name varchar,
    in_reply_to_user_id     varchar,
    in_reply_to_status_id   varchar,
    is_quote_status         varchar,    --NOTE: will need to cast to bool when pushed to core
    quoted_status_id        varchar,
    retweeted_status_id     varchar,
    created_dt              timestamp not null,
    extracted_dt            timestamp not null
) distribute by hash(tweet_id);
GRANT SELECT ON public.social_sentiment_stg_twitter to public;


DROP TABLE IF EXISTS public.social_sentiment_stg_sail;
create fact table public.social_sentiment_stg_sail
(   tweet_text              varchar not null,
    parsed_text             varchar not null,
    text_length             integer,
    user_id                 decimal(19,0),
    published_date          varchar,    --NOTE: will need to cast to timestamp when pushed to core
    sentiment               varchar,
    tweet_id                decimal(19,0) not null,
    prediction_prob         decimal(7,6),
    model_file              varchar,
    model_mod_dt            timestamp,
    processed_dt            timestamp
) distribute by hash(tweet_id);
GRANT SELECT ON public.social_sentiment_stg_sail to public;


DROP TABLE IF EXISTS public.social_sentiment_stg_user;
create fact table public.social_sentiment_stg_user
(   user_id                     decimal(19,0) not null,
    name                        varchar,
    screen_name                 varchar,
    has_extended_profile        varchar,--NOTE: will need to cast to bool when pushed to core
    contributors_enabled        varchar,--NOTE: will need to cast to bool when pushed to core
    verified                    varchar,--NOTE: will need to cast to bool when pushed to core
    protected                   varchar,--NOTE: will need to cast to bool when pushed to core
    is_translator               varchar,--NOTE: will need to cast to bool when pushed to core
    lang                        varchar,
    location                    varchar,
    geo_enabled                 varchar,--NOTE: will need to cast to bool when pushed to core

    default_profile_image       varchar,--NOTE: will need to cast to bool when pushed to core
    profile_image_url           varchar,

    description                 varchar,
    url                         varchar,
    entities                    varchar,

    statuses_count              bigint,
    favourites_count            bigint,
    followers_count             bigint,
    friends_count               bigint,
    listed_count                bigint,

    utc_offset                  varchar,--NOTE: will need to cast to int when pushed to core
    time_zone                   varchar,
    created_dt                  timestamp not null,
    extracted_dt                timestamp not null
    
) distribute by hash(user_id);
GRANT SELECT ON public.social_sentiment_stg_user to public;




-- create core tables
DROP TABLE IF EXISTS public.social_sentiment_brandwatch;
create fact table public.social_sentiment_brandwatch
(   queryId                 decimal(19,0) not null,
    queryName               varchar,
    resourceId              varchar,
    resourceType            varchar,
    
    tags                    varchar,
    categoryDetails         varchar,
    
    continent               varchar,
	continentCode           varchar,
	country                 varchar,
	countryCode             varchar,
	state                   varchar,
	stateCode               varchar,
	county                  varchar,
	countyCode              varchar,
	city                    varchar,
	cityCode                varchar,
	latitude                varchar,
	longitude               varchar,
    
	author                  varchar,
	authorContinent         varchar,
	authorContinentCode     varchar,
	authorCountry           varchar,
	authorCountryCode       varchar,
	authorState             varchar,
	authorStateCode         varchar,
	authorCounty            varchar,
	authorCountyCode        varchar,
	authorCity              varchar,
	authorCityCode          varchar,
	    
    fullname                varchar,
    gender                  varchar,
    avatarUrl               varchar,
    professions             varchar,
    interest                varchar,
    
    domain                  varchar,
    pageType                varchar,
    accountType             varchar,
    url                     varchar,
    mediaUrls               varchar,
    
    sentiment               varchar,
    fullText                varchar,
    language                varchar,
    wordCount               integer,
    
    facebookAuthorId        varchar,
    facebookRole            varchar,
    facebookSubtype         varchar,
    facebookShares          integer,
    facebookLikes           integer,
    facebookComments        integer,
    
    tweet_id                decimal(19,0) not null,
    twitterAuthorId         varchar,
    twitterVerified         varchar,
    twitterRole             varchar,
    twitterFollowing        integer,
    twitterFollowers        integer,
    twitterRetweets         integer,
    twitterPostCount        integer,
    twitterReplyCount       integer,
    
    forumViews              integer,
    forumPosts              integer,
    blogComments            integer,
    status                  varchar,
    retweetOf               varchar,
    replyTo                 varchar,
    insightsHashtag         varchar,
    insightsMentioned       varchar,
    influence               varchar,
    
    averageDurationOfVisit  varchar,
    averageVisits           varchar,
    pagesPerVisit           varchar,
    monthlyVisitors         varchar,
    percentMaleVisitors     integer,
    percentFemaleVisitors   integer,
    
    engagement              varchar,
    importanceAmplification integer,
    importanceReach         integer,
    impact                  integer,
    impressions             varchar,
    mozRank                 varchar,
    outreach                varchar,
    reach                   varchar,
    starred                 varchar,
    trackedLinkClicks       integer,
    trackedLinks            varchar,

    editorialValueEUR       varchar,
    editorialValueGBP       varchar,
    editorialValueUSD       varchar,

    assignment              varchar,
    lastAssignmentDate      varchar,
    backlinks               varchar,
    checked                 varchar,
    "date"                  varchar,
    displayUrls             varchar,
    expandedUrls            varchar,
    matchPositions          varchar,
    noteIds                 varchar,
    priority                varchar,
    shortUrls               varchar,
    snippet                 varchar,

    threadId                varchar,
    threadAuthor            varchar,
    threadEntryType         varchar,
    threadURL               varchar,
    threadCreated           varchar,
    
    extracted_dt            timestamp not null,
    added_dt                timestamp default CURRENT_TIMESTAMP,
    updated_dt              timestamp default CURRENT_TIMESTAMP
 ) distribute by hash(tweet_id);
GRANT SELECT ON public.social_sentiment_brandwatch to public;


DROP TABLE IF EXISTS public.social_sentiment_twitter;
create fact table public.social_sentiment_twitter
(   tweet_id                decimal(19,0) not null,
    tweet_text              varchar,
    tweet_text_uni_esc      varchar,
    lang                    varchar,
    truncated               bool,
    possibly_sensitive      bool,
    extended_entities       varchar,
    
    favorite_count          bigint,
    retweet_count           bigint,
    
    coordinates             varchar,
    place                   varchar,
    country                 varchar,
    country_code            varchar,
    place_type              varchar,
    place_name              varchar,
    place_full_name         varchar,
    
    source                  varchar,
    contributors            varchar,
    user_id                 decimal(19,0),
    in_reply_to_screen_name varchar,
    in_reply_to_user_id     varchar,
    in_reply_to_status_id   varchar,
    is_quote_status         bool,
    quoted_status_id        varchar,
    retweeted_status_id     varchar,
    created_dt              timestamp not null,
    extracted_dt            timestamp not null,
    added_dt                timestamp default CURRENT_TIMESTAMP,
    updated_dt              timestamp default CURRENT_TIMESTAMP
) distribute by hash(tweet_id);
GRANT SELECT ON public.social_sentiment_twitter to public;


DROP TABLE IF EXISTS public.social_sentiment_sail;
create fact table public.social_sentiment_sail
(   tweet_text              varchar not null,
    parsed_text             varchar not null,
    text_length             integer,
    user_id                 decimal(19,0),
    published_date          timestamp,
    sentiment               varchar,
    tweet_id                decimal(19,0) not null,
    prediction_prob         decimal(7,6),
    model_file              varchar,
    model_mod_dt            timestamp,
    processed_dt            timestamp,
    added_dt                timestamp default CURRENT_TIMESTAMP,
    updated_dt              timestamp default CURRENT_TIMESTAMP
) distribute by hash(tweet_id);
GRANT SELECT ON public.social_sentiment_sail to public;


DROP TABLE IF EXISTS public.social_sentiment_user;
create fact table public.social_sentiment_user
(   user_id                     decimal(19,0) not null,
    name                        varchar,
    screen_name                 varchar,
    has_extended_profile        bool,
    contributors_enabled        bool,
    verified                    bool,
    protected                   bool,
    is_translator               bool,
    lang                        varchar,
    location                    varchar,
    geo_enabled                 bool,

    default_profile_image       bool,
    profile_image_url           varchar,

    description                 varchar,
    url                         varchar,
    entities                    varchar,

    statuses_count              bigint,
    favourites_count            bigint,
    followers_count             bigint,
    friends_count               bigint,
    listed_count                bigint,

    utc_offset                  integer,
    time_zone                   varchar,
    created_dt                  timestamp not null,
    extracted_dt                timestamp not null,
    added_dt                    timestamp default CURRENT_TIMESTAMP,
    updated_dt                  timestamp default CURRENT_TIMESTAMP
) distribute by hash(user_id);
GRANT SELECT ON public.social_sentiment_user to public;