"""
@author: Elias Wood (owns13927@yahoo.com)
Created: 2016-01-13
Takes brandwatch and twitter exports and generates the sail input file,
placing them in the correct sub input folder. Shouldn't need to use,
but just in case ;)

CHANGELOG
2016-02-02 v1.0.0: Elias Wood
    first version to work with ../autosail.py by using brandwatch.csv amd 
    twitter.csv to create [1st arg or pwd]/[config name]/[config name].*\.csv
    input files for SAIL to use. 
2016-01-14 v0.1.1: Elias Wood
    first argument is taken has the path to search for twitter csv files
    Also, added error handling for if MyLoggingBase isn't found
2016-01-12 v0.1.0: Elias Wood
    First version!
"""

import csv
import os

import sys
sys.path.append('..')
try:
    from myloggingbase import MyLoggingBase
except ImportError:
    import logging
    class MyLoggingBase(object):
        logger = None
        def __init__(self,name=None,*args,**keys):
            self.logger = logging.getLogger(self.__class__.__name__
                                            if name is None else name)

        @staticmethod
        def init_logging(*args,**keys):
            logging.basicConfig(level=logging.DEBUG)
            
        

if __name__ == '__main__':
    MyLoggingBase.init_logging(file_log_lvl=None)
    m = MyLoggingBase(name='twittertosail')
    
    d = os.path.realpath( sys.argv[1] if len(sys.argv) > 1 else '' )
    
    m.logger.info('working with dir %s',d)
    
    files_processed = 0
    rows_processed = 0
    
    # --- get mapping
    # get config query --> name
    query_to_name = dict() # [queryId] --> config name
    p = MyLoggingBase.get_resource_fd('../SAILconfigs')
    if not os.path.exists(p): os.mkdir(p)
    for ids in map(lambda t: {a:t[1] for a in t[0]},
                     ((d['queryIds'].split(','),d['name']) for d in \
                      (MyLoggingBase.read_file_key_values(os.path.join(p,f))
                       for f in os.listdir(p)) if d['queryIds'])):
        query_to_name.update(ids)
    
    # go through each brandwatch csv
    tweet_to_name = {}
    m.logger.info('getting tweet to name mapping...')
    for fpath in (os.path.join(d,f) for f in os.listdir(d)):
        if not os.path.split(fpath)[1].startswith('brandwatch'): continue
        m.logger.debug('processing %s',fpath)
        with open(fpath,'rb') as reader:
            cr = csv.reader(reader,delimiter='\t',quotechar='"',
                            doublequote=True,skipinitialspace=True)
            cr.next() # skip headers
            for r in cr: tweet_to_name[r[49]]=query_to_name.get(r[0],'default')
    
    # create output
    m.logger.info('creating output...')
    writers = dict()
    opens = []
    for f in os.listdir(d):
        
        fpath = os.path.join(d,f) 
        
        if not os.path.isfile(fpath) or not f.startswith('twitter'): continue
        
        m.logger.info('processing "%s"...',fpath)
        with open(fpath,'rb') as reader:
            cr = csv.reader(reader,delimiter='\t',quotechar='"',
                            doublequote=True,skipinitialspace=True)
            
            cr.next() # skip headers
            
            for r in cr:
                rows_processed += 1
                t = r[1].decode('utf8').encode('ascii','ignore')
                if t.endswith('\\'): t+= ' '
                
                name = tweet_to_name.get(r[0],'unknown')
                if name not in writers:
                    # make folder if needed
                    if not os.path.exists(os.path.join(d,name)):
                        os.mkdir(os.path.join(d,name))
                        m.logger.info('folder created: %s',os.path.join(d,name))
                        
                    opens.append(open(os.path.join(d,name,name+'.csv'),'wb'))
                    m.logger.info('creating: %s',os.path.join(d,name,name+'.csv'))
                    cw = writers[name] = csv.writer(opens[-1],delimiter='\t',quotechar='"',
                            doublequote=True,skipinitialspace=True,quoting=csv.QUOTE_ALL)
                    
                    cw.writerow(('url','user','tweet_text','published_date','extracted_dt'))
                    cw.writerow((r[0],r[18],t,r[25],r[26]))
                    
                else: writers[name].writerow((r[0],r[18],t,r[25],r[26]))
            
        files_processed += 1
    
    # close all open csv writers
    for w in opens: w.close()
    m.logger.info('files processed: %d',files_processed)
    m.logger.info('rows processed (w/o headers): %d',rows_processed)
    
