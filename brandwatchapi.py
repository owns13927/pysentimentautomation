"""
@author: Elias Wood (owns13927@yahoo.com)
Created: 2015-12-14
A basic Brand Watch API wrapper 
"""

import requests
import logging
import time
from datetime import datetime
logging.getLogger('requests').setLevel(logging.WARNING)

from myloggingbase import MyLoggingBase
from myexceptions import TooManyRequests,MaxRetryLimit,InvalidToken

class BrandWatchAPI(MyLoggingBase):
    __version__ = '0.1.1'
    #===========================================================================
    # Variables
    #===========================================================================
    _session = None
    __token = None
    __project = None
    __time_last_call = None
    _proxies = None
    
    #===========================================================================
    # Static Variables
    #===========================================================================
    URL_BASE = 'https://newapi.brandwatch.com'
    API_CALL_TIME_LIMIT = 1.5
    # page size max is 5000 - go any higher and you'll get an error:
    # --- {u'errors': [{u'code': 0, u'message': u'Bad request: pageSize=9999 
    #                       expected int. must be less than or equal to 5000'}]}
    QUERY_PARAMS = {'pageSize':5000,'pageType':'twitter', 
                    'orderBy':'date','orderDirection':'desc'}
    DEFAULT_WAIT_DURATION = 900 # 15 minutes
    RETRY_CONNECTION_TIME = 60 # wait time if connection issue.
    WAIT_SALT = 15 # wait an extra X seconds for the window to open
    MAX_TRY_COUNT = 6 # make times to re-try a request
    
    #===========================================================================
    # Tracked Metrics
    #===========================================================================
    __requests_get_count = None
    __api_request_count = None
    __errors_returned = None
    __start_time = None
    __end_time = None
    
    def __init__(self,token,project,*args,**keys):
        super(BrandWatchAPI, self).__init__(*args,**keys)
        
        # setup proxy
        self._proxies = dict()
        if 'proxies' in keys:
            if isinstance(keys['proxies'],dict):
                self.set_proxies(keys['proxies'])
            else: self.logger.warning('proxies must be a dictionary, e.g. http:http://user:pass@my.proxy.com:123')
        
        self.set_token(token)
        self.set_project(project)
        self.__time_last_call = time.clock()-9
        
        # start the connection session
        self.reset_session()
        
    #===========================================================================
    # Session
    #===========================================================================
    def reset_session(self,log_stats=False):
        """
        closes the current session (if one is already up)
        log_stats: will call log_summary_info(...)
                   before reseting the stats for the new session
        """
        if self.session_is_open():
            self.close_session()
            # log stats if asked
            if log_stats: self.log_summary_info()
        
        # open a new session
        self._session = requests.Session()
        self.__start_time = time.clock()
        self.__end_time = None
        if self.__token:
            self._session.headers['Authorization'] = 'bearer'+self.__token
        
        # set metrics start
        self.__requests_get_count = 0
        self.__api_request_count = 0
        self.__errors_returned = 0
        
    def session_is_open(self):
        """ determines if the session is open (not None) """
        return isinstance(self._session, requests.Session)
    
    def close_session(self):
        """closes session"""
        if self.session_is_open():
            self.__end_time = time.clock()
            self._session.close()
            self._session = None
    
    def close(self,*args,**keys):
        """closes session & anything else..."""
        self.close_session()
    
    
    #===========================================================================
    # Getters / Setters
    #===========================================================================
    def set_token(self,token):
        self.__token = token
    def get_token(self):
        return self.__token
    
    def set_project(self,project):
        self.__project = project
    def get_project(self):
        return self.__project
    
    def update_proxies(self,proxies):
        """updates the proxies used by the session"""
        self._proxies.update(proxies)
        
    def get_proxies(self):
        """returns a copy of the proxies being used by the session"""
        return self._proxies.copy()
    
    def set_proxies(self,proxies):
        """set the proxies used by the session"""
        self._proxies = proxies
        
    #===========================================================================
    # Make an API call
    #===========================================================================
    def _api_parse(self,r):
        try: j = r.json()
        except Exception as e:          # TO JSON ERROR?
            j = {}
            self.logger.warn('request error - %s: %r',
                             e.__class__.__name__,e)
            self.__errors_returned += 1
        else:
            if 'error' in j:
                self.logger.warn('server response: %r',j)
                if j['error'] == 'invalid_token':
                    raise InvalidToken(j.get('error_description','invalid token'))
            
            elif 'errors' in j:           # API ERROR?
                self.logger.critical('%r',j)
                raise Exception(j['errors'][0]['message'])
        return j
    
    def _make_api_call(self,url,**params):
        """
        """
        self.__api_request_count += 1
        self.logger.debug('%03d:%s %r',self.__api_request_count,
                          url,params)
        
        
        try_count = 0
        while True:
            try_count += 1
            try:
                if try_count > self.MAX_TRY_COUNT: raise MaxRetryLimit(
                    'max retry limit of %s reached' % (self.MAX_TRY_COUNT,))
                
                extracted_dt = r = j = None
                    
                # make the call
                self.__requests_get_count += 1
                r = self._session.get(url,params=params,proxies=self._proxies)
                self.__time_last_call = time.clock()
            
                # parse timestamp
                extracted_dt = self.datetime_to_timestamp(
                                        datetime.strptime(
                                        r.headers['date'],
                                        '%a, %d %b %Y %H:%M:%S %Z'))    
                
                # did we hit the rate limit 'Too Many Requests'?
                if r.status_code == 429:
                    raise TooManyRequests('HTTP 429: Too Many Requests')
                
                # error handling...
                #if r.status_code == 200: pass
                #else: pass
                
                # parse to json and handle errors
                j = self._api_parse(r)
                
            #=======================================================================
            # Error Handling
            #=======================================================================
            # if it's a connection issue, wait some and try again.
            except requests.ConnectionError, e:
                #RETRY_CONNECTION_TIME
                self.logger.warning('ConnectionError - trying after %ds. %s',
                                    self.RETRY_CONNECTION_TIME,e)
                self.wait_to_retry(try_count,self.RETRY_CONNECTION_TIME)
            
            except (requests.RequestException,requests.Timeout,
                    requests.URLRequired,requests.TooManyRedirects,
                    requests.HTTPError) as e:
                raise e
            
            else: break
            
        return extracted_dt, j
    
    def make_api_call(self,path,**params):
        """
        """
        # fix path if needed
        if path.startswith('/'): path = path[1:]
        
        # set url
        url = (path if path.startswith(self.URL_BASE) else
               '{}/projects/{}/{}'.format(self.URL_BASE,self.__project,path))
        
        # remove None params is exists
        if None in params.values():
            params = {k:v for k,v in params.iteritems() if v is not None}
            
        # sleep if needed (Brand Watch API calls must be 1 sec apart
        t = time.clock()-self.__time_last_call
        if t < self.API_CALL_TIME_LIMIT:
            time.sleep(self.API_CALL_TIME_LIMIT-t)
        
        extracted_dt , j  = self._make_api_call(url,**params)
        
        if j.get('resultsPageSize',-1) != -1 and \
                j.get('resultsTotal',0) > j.get('resultsPageSize',1):
            self.logger.warning(
                'more results returned than allowed in one page - %r',
                {k:(v if not isinstance(v,(dict,tuple,list)) else '...')
                 for k,v in j.iteritems()})
            
        for d in j.get('results',tuple()):
            d['extracted_dt'] = extracted_dt
        
        return extracted_dt,j.get('results')
    ''
    #===========================================================================
    # Functions to override!
    #===========================================================================
    def wait_to_retry(self,try_count,t=None):
        """ called whenever the crawler needs to wait, usually b/c
        a rate limit was reached.
        """
        if t is None: t = self.DEFAULT_WAIT_DURATION
        self.logger.info('waiting {} seconds - try {}'.format(t,try_count))
        self.before_sleeping(try_count)
        time.sleep(t)
        self.after_sleeping(try_count)
        
    def before_sleeping(self,try_count):
        """ called in self.wait_to_retry before sleeping """
        pass
    
    def after_sleeping(self,try_count):
        """ called in self.wait_to_retry after sleeping """
        pass
    ''
    #===========================================================================
    # Custom methods
    #===========================================================================
    def get_queries(self):
        """returns all queries in the current project"""
        et,l = self.make_api_call('queries.json')
        self.logger.info('%d queries extracted on %s',len(l),et)
        return l
    
    def get_tweets_from_query(self,query_id,startDate,endDate):
        """
        """
        d = dict(queryId=query_id,startDate=startDate,endDate=endDate,
                 **self.QUERY_PARAMS)
        
        et,l = self.make_api_call('data/mentions.json',**d)
        
        self.logger.info('%d tweets from query %s [%s,%s] extracted %s',
                          len(l),query_id,startDate,endDate,et)
        
        for d in l:
            # set tweetId
            if d.get('pageType') == 'twitter' and d.has_key('url'):
                d['tweet_id'] = d['url'][d['url'].rfind('/')+1:]
            
        return l
    
    #def get_user(self): return '/user.json'
     
    #===========================================================================
    # get into summary
    #===========================================================================
    def _get_summary_info(self):
        """ add count of api calls and errors
        """
        a = super(BrandWatchAPI, self)._get_summary_info() 
        a.extend(('get requests: {:,}'.format(self.__requests_get_count),
                  'api requests: {:,}'.format(self.__api_request_count),
                  'error count: {:,}'.format(self.__errors_returned),
                  'session open: {:,} min'.format(((self.__end_time if self.__end_time
                                                    else time.clock()) - self.__start_time
                                                   )/60.0 if self.__start_time is not None
                                                  else -1)
                ))
        return a
    
    
#===============================================================================
# MAIN
#===============================================================================
if __name__ == '__main__':
    #try: from tests import test_fbapibase
    #except ImportError: print 'no test for fbapibase'
    #else: test_fbapibase.run_test()
    
    # setup brandwatch api
    # get cred from text file
    MyLoggingBase.init_logging(file_log_lvl=None)
    
    # get proxy info
    proxies = dict()
    d = MyLoggingBase.read_file_key_values(
        MyLoggingBase.get_resource_fd('proxy.txt')
    )
    if d.pop('enabled','1') == '1':
        proxies.update(d)
    
    cred = MyLoggingBase.read_file_key_values(
                MyLoggingBase.get_resource_fd('brandwatch_cred.txt'),
                'token','project')
    
    bwapi = BrandWatchAPI(token=cred['token'],project=cred['project'],
                          proxies=proxies)
    del proxies,cred
    
    from myjson2csv import MyJSON2CSV
    
    ''' # Get Queries
    bw = MyJSON2CSV(filename=MyLoggingBase.get_output_fd('brandwatch_queries.txt'))
    bw.set_separator('\t')
    bw.set_qouting_all()
    bw.set_headers('id','name','type','description',
                   'includedTerms','excludedTerms','contextTerms','highlightTerms',
                   'twitterScreenName','averageMonthlyMentions','languages','industry',
                   'creationDate','lastModificationDate','lastModifiedUsername','extracted_dt')
    
    for query in bwapi.get_queries():
        bw.write_json_object(query)
    
    bwapi.close()
    bwapi.log_summary_info()
    
    bw.close()
    bw.log_summary_info()
    
    ''' # Get mentions
    bw = MyJSON2CSV(filename=MyLoggingBase.get_output_fd('brandwatch_metions.csv'))
    bw.set_separator('\t')
    bw.set_qouting_all()
    bw.set_headers('queryId','queryName','resourceId','resourceType','tags',
                   'categoryDetails','continent','continentCode','country',
                   'countryCode','state','stateCode','county','countyCode',
                   'city','cityCode','latitude','longitude','author',
                   'authorContinent','authorContinentCode','authorCountry',
                   'authorCountryCode','authorState','authorStateCode',
                   'authorCounty','authorCountyCode','authorCity',
                   'authorCityCode','fullname','gender','avatarUrl',
                   'professions','interest','domain','pageType','accountType',
                   'url','mediaUrls','sentiment','fullText','language',
                   'wordCount','facebookAuthorId','facebookRole',
                   'facebookSubtype','facebookShares','facebookLikes',
                   'facebookComments','tweet_id','twitterAuthorId',
                   'twitterVerified','twitterRole','twitterFollowing',
                   'twitterFollowers','twitterRetweets','twitterPostCount',
                   'twitterReplyCount','forumViews','forumPosts','blogComments',
                   'status','retweetOf','replyTo','insightsHashtag',
                   'insightsMentioned','influence','averageDurationOfVisit',
                   'averageVisits','pagesPerVisit','monthlyVisitors',
                   'percentMaleVisitors','percentFemaleVisitors','engagement',
                   'importanceAmplification','importanceReach','impact',
                   'impressions','mozRank','outreach','reach','starred',
                   'trackedLinkClicks','trackedLinks','editorialValueEUR',
                   'editorialValueGBP','editorialValueUSD','assignment',
                   'lastAssignmentDate','backlinks','checked','date',
                   'displayUrls','expandedUrls','matchPositions','noteIds',
                   'priority','shortUrls','snippet','threadId','threadAuthor',
                   'threadEntryType','threadURL','threadCreated','extracted_dt')
    
    startDate = bwapi.get_current_timestamp(utc=True,
                                            add_sec=-3*60*60*24*30)
    
    # endDate just needs to be in the future! (a little over a day)
    endDate = bwapi.get_current_timestamp(utc=True, add_sec=99999)
    
    queryId = MyLoggingBase.read_file_key_values(
                MyLoggingBase.get_resource_fd('ztest_brandwatch_query_id.txt'),
                'queryId')['queryId']
    
    for mtweet in bwapi.get_tweets_from_query(queryId,startDate=startDate,endDate=endDate):
        bw.write_json_object(mtweet)
    
    bwapi.close()
    bwapi.log_summary_info()
    
    bw.close()
    bw.log_summary_info()
    #'''
