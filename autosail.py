"""
@author: Elias Wood (owns13927@yahoo.com)
Created: 2016-02-01
process SAIL input (assumed in a specific format - 
    [input folder]/[config name]/[config name].*\.csv),
    move to upload folder, and clean. 

CHANGELOG
2016-02-02 v0.1.0: Elias Wood
    testing first version
"""
from datetime import datetime
import subprocess as sp
import os
import shutil
import csv
from myloggingbase import MyLoggingBase
from myjson2csv import MyJSON2CSV

class AutoSAIL(MyLoggingBase):
    __version__ = '0.1.0'
    #===========================================================================
    # Variables
    #===========================================================================
    sail_input_fd = None
    sail_output_fd = None
    sail_exe = None
    dest_fd = None
    configs = None
    
    #===========================================================================
    # Static Variables
    #===========================================================================
    sail_sub_fd = 'original'
    _headers = ('tweet_text','parsed_text','text_length','user_id',
                'published_date','sentiment','tweet_id','prediction_prob',
                'model_file','model_mod_dt','processed_dt')
    
    #===========================================================================
    # Tracked Metrics
    #===========================================================================
    __files_processed = None
    __deleted_input_files = None
    __rows_processed = None
    __rows_by_config = None
    
    def __init__(self,*args,**keys):
        super(AutoSAIL,self).__init__(self,*args,**keys)
        
        # load locations
        a = self.read_file_key_values(self.get_resource_fd('folders.txt'),
                'uploadFolder','SAILInputFolder','SAILOutputFolder','SAILExe')
        
        self.sail_input_fd = a['SAILInputFolder']
        self.sail_output_fd = a['SAILOutputFolder']
        self.sail_exe = a['SAILExe']
        self.dest_fd = a['uploadFolder']
        del a
        
        # load configs
        p = self.get_resource_fd('SAILconfigs')
        if not os.path.exists(p): os.mkdir(p)
        self.configs = dict()
        for config in (self.read_file_key_values(os.path.join(p,filepath),
                                                 'name','model')
                       for filepath in os.listdir(p)):
            self.configs[config['name']] = config
        
        # init metrics
        self.__files_processed = 0
        self.__deleted_input_files = 0
        self.__rows_processed = 0
        self.__rows_by_config = dict()
        
    def start(self):
        self.run()
    
    def run(self):
        self.run_SAIL() # process the input files
        files = self.move_to_upload() # combine and move to upload fd
        self.cleanup_folders(files) # delete processed files
        
    def run_SAIL(self):
        # for all in sail input folder
        for name in os.listdir(self.sail_input_fd):
            input_fd = os.path.join(self.sail_input_fd,name)
            # if it's a not a directory or an empty one, skip
            if not os.path.isdir(input_fd) or not os.listdir(input_fd): continue
            
            self.logger.info('processing input folder %s ...',input_fd)
            # process through sail
            model = self.configs.get(name,{}).get('model')
            cmd = [self.sail_exe,input_fd,self.sail_output_fd]
            if model: cmd.append(model)
            self.logger.debug('%s',cmd)
            try: sp.check_call(cmd) # if return code != 0, error is raised
            except sp.CalledProcessError, e:
                self.logger.critical('%r\n\tcmd=%s,\n\targs=%s,\n\tmessage=%s'+
                                     ',\n\toutput=%s,\n\treturncode=%s,\n\t',
                                     e,e.cmd,e.args,e.message,
                                     e.output,e.returncode)
            #break
    
    def move_to_upload(self):
        """combine all SAIL output files and adding the 3 columns.
        Returns the processed filenames w/o exts
        """
        # open the writer to combined
        cw = MyJSON2CSV(filename=os.path.join(self.dest_fd,'sail.csv'))
        cw.set_separator('\t')
        cw.set_qouting_all()
        cw.set_headers(*self._headers) # doesn't do anything since we don't use JSON
        
        # prep for processing
        files = []
        output_fd = os.path.join(self.sail_output_fd,self.sail_sub_fd)
        self.logger.info('processing sail output files in %s',output_fd)
        
        # for each output file
        for fname in os.listdir(output_fd):
            if not fname.endswith('.tsv'): continue
            
            files.append(os.path.splitext(fname)[0]) # remove extensions
            fpath = os.path.join(output_fd,fname)
            
            # init added columns
            name = [k for k in self.configs if k in fname][0]
            model_file = self.configs[name]['model']
            model_mod_dt = self.datetime_to_timestamp(datetime.fromtimestamp(
                                    os.path.getmtime(model_file)))
            processed_dt = self.datetime_to_timestamp(datetime.fromtimestamp(
                                    os.path.getmtime(fpath)))
            
            self.logger.debug('adding %s modified %s ...',fname,processed_dt)
            
            with open(fpath,'rb') as reader:
                cr = csv.reader(reader,delimiter='\t',
                                quotechar="'",escapechar='\\')
                
                cr.next() # skip headers
                """ process each row, only saving what's needed
                tweet_text      5  varchar not null,
                parsed_text     7  varchar not null,  #39,40
                text_length     8  integer,
                user_id         10 varchar,
                published_date  11 varchar, (will transform to timestamp)
                sentiment       43 varchar,
                tweet_id        44 varchar,
                prediction_prob 50 decimal(7,6),
                model_file         varchar,
                model_mod_dt       timestamp,
                processed_dt       timestamp
                """
                c = 0
                for row in ((r[5],r[7].decode('ascii','replace'),
                             r[8],'' if r[10]=='?' else r[10],
                             '' if r[11]=='?' else r[11],r[43],r[44],r[50],
                             model_file,model_mod_dt,processed_dt)
                            for r in cr):
                                              
                    cw.writerow(row)
                    c += 1
                
                # track metrics
                if name not in self.__rows_by_config:
                    self.__rows_by_config[name] = 0
                self.__rows_by_config[name] += c 
                self.__rows_processed += c    
                self.__files_processed += 1
                
        cw.close() # close writer
        cw.log_summary_info() # log details
        
        return files
    
    def cleanup_folders(self,output_files):
        # clean up sail!
        self.logger.info('clean up left-over sail files/folders in %s',
                         self.sail_output_fd)
        for f in os.listdir(self.sail_output_fd):
            item = os.path.join(self.sail_output_fd,f)
            if os.path.isdir(item):
                shutil.rmtree(item,ignore_errors=True)
            else: os.remove(item)
            
        # delete process input files if exists. log has warning if not.
        # get all input files
        input_files = dict()
        for root, dirs, files in os.walk(self.sail_input_fd): #@UnusedVariable
            input_files.update({os.path.splitext(f)[0]:os.path.join(root,f)
                                for f in files})
        # delete matches
        for fname in output_files:
            fpath = input_files.pop(fname,'')
            if os.path.exists(fpath):
                self.logger.debug('deleting processed SAIL input, "%s"',fpath)
                try: os.remove(fpath)
                except Exception, e: self.logger.critical(
                                'failed to delete input file, %s - %r',fpath,e)
                else: self.__deleted_input_files += 1
            # log if the input file DNE.  it should...
            else: self.logger.warn(
                'SAIL output file found, but matching input "%s" DNE.',fpath)
        
        # did sail not process some input files?
        comm_len = len(os.path.commonprefix(input_files.values()))
        if input_files:
            self.logger.warning('%d csv files not processed by SAIL! %s',
                                len(input_files),
                                [i[comm_len:] for i in input_files.itervalues()])
    
    def stop(self):
        raise Exception('Not Implemented')
    
    def get_summary_info(self):
        a = super(AutoSAIL,self).get_summary_info()
        a.extend(('files processed: {:,}'.format(self.__files_processed),
            'input files deleted: {:,}'.format(self.__deleted_input_files),
            'rows processed (w/o headers): {:,}'.format(self.__rows_processed))+
             tuple('{} rows: {:,}'.format(k,v) 
                   for k,v in self.__rows_by_config.iteritems()))
        return a  
''
#===============================================================================
# MAIN
#===============================================================================
def main():
    #""" setup logging
    MyLoggingBase.init_logging(
        file_log_name=MyLoggingBase.get_output_fd('autosail{}.log'.format(
            MyLoggingBase.get_current_timestamp(for_file=True)
        ))
    )
    """
    MyLoggingBase.init_logging(file_log_lvl=None)
    #"""
    # init class
    a = AutoSAIL()
    # start working
    a.start()
    # log details
    a.log_summary_info()
    
if __name__ == '__main__':
    main()



""" old code for processing
    def process_sail_output_files(self):
        # process sail!
        files = []
        d = os.path.join(self.sail_output_fd,self.sail_sub_fd)
        self.logger.debug('processing sail files in %s',d)
        # check the sub output folder exists; if not, nothing to do 
        if not os.path.exists(d):
            self.logger.warning("sail output folder DNE. This happens if "+
                "sail hasn't ran since the output folder was last cleaned.")
            return;
        
        for fname in os.listdir(d):
            # only process the tsv files (tab separated files - ha)
            if not fname.endswith('.tsv'): continue
            
            # track the files we process to also move matching input files
            files.append(os.path.splitext(fname)[0]) # remove extensions
            dest_file = os.path.join(self.dest_fd,
                                     files[-1].replace('twitter','sail')+'.csv')
            
            # move file to dest_fd, keeping needed columns
            with open(os.path.join(d,fname),'rb') as reader:
                
                cr = csv.reader(reader,delimiter='\t',
                                quotechar="'",escapechar='\\')
                
                cw = MyJSON2CSV(filename=dest_file)
                cw.set_separator('\t')
                cw.set_qouting_all()
                
                self.__files_processed += 1
                
                # write headers
                cw.set_headers(*self._headers) #cw.writerow(self._headers)
                cr.next() # skip headers in the reader
                self.logger.debug('transforming %s to %s',fname,cw.get_filename())
                
                ''' process each row, only saving what's needed
                tweet_text      5  varchar not null,
                parsed_text     7  varchar not null,  #39,40
                text_length     8  integer,
                user_id         10 varchar,
                published_date  11 varchar, (will transform to timestamp)
                sentiment       43 varchar,
                tweet_id        44 varchar,
                prediction_prob 50 decimal(7,6)'''
                
                for row in ((r[5],r[7].decode('ascii','replace'),
                             r[8],'' if r[10]=='?' else r[10],
                             '' if r[11]=='?' else r[11],r[43],r[44],r[50])
                            for r in cr):
                                              
                    cw.writerow(row)
                    self.__rows_processed += 1
                
                cw.close() # close writer
        
        # if file in Features output folder but not original, that means
        # there was an error with that input file. 
            
        # clean up sail!
        self.logger.debug('clean up left-over sail files/folders in %s',
                          self.sail_output_fd)
        for f in os.listdir(self.sail_output_fd):
            item = os.path.join(self.sail_output_fd,f)
            if os.path.isdir(item):
                shutil.rmtree(item,ignore_errors=True)
            else: os.remove(item)
            
        # delete process input files if exists. log has warning if not.
        for fname in files:
            fpath = os.path.join(self.sail_input_fd,fname+'.csv')
            if os.path.exists(fpath):
                self.logger.debug('deleting processed SAIL input, "%s"',fpath)
                try: os.remove(fpath)
                except Exception, e: self.logger.critical(
                                'failed to delete input file, %s - %r',fpath,e)
            # log if the input file DNE.  it should...
            else: self.logger.warn(
                'SAIL output file found, but matching input "%s" DNE.',fpath)
        # done
"""