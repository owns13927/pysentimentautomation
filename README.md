# README #

This application gets all queries for a [Brandwatch](https://www.brandwatch.com/api/) project.
It then gets all tweets under a query, remembering when it last grabbed for delta loads.
Next, using [Twitter's API](https://dev.twitter.com/overview/documentation), it pulls further twitter data.
[SAIL](https://github.com/uiuc-ischool-scanr/SAIL) is then used to predict sentiment.
SAIL's output is finally properly formatted and prepared for upload into [Terradata Aster](http://developer.teradata.com/aster).

### What is this repository for? ###

* Quick summary

* Version 1.0.0

### How do I get set up? ###

To get setup, there are several files that will need to be created in the ./resources/ folder.  Below describes each and an example or what the file content should look like.

#### folders.txt ####

Contains folder paths used through the application flow. see 

* uploadFolder - where files go to be uploaded to the database
* SAILInputFolder - SAIL's input folder
* SAILOutputFolder - SAIL's output folder
* SAILExe

i.e. 

#### brandwatch_cred.txt ####

i.e. 

#### twitter_cred.txt ####

i.e. 

#### proxy.txt ####

i.e. 

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* For comments/questions/concerns please contact the developer, Elias at owns13927@yahoo.com.

### Changelog ###
#### 2015-02-02 - 1.0.0 ####
* big changes! added more brandwatch and twitter fields and created a separate export for users
* main.py, resouces/twittertosail.py, and autosail.py (to replace sailtocsv.py) changed to handle custom model files to be used by SAIL.
* more progress with the README.md!
* see individual files for more details

#### 2015-01-12 - 0.5.0 ####
* big change! main.py and sailtocsv.py changed. Separate export (view of twitter export) just for SAIL.  if "some tweet :-\" is the full_text, SAIL will fail.
* added resource "twittertosail.py" to generate the SAIL input file from the twitter export.
 
#### 2015-01-12 - 0.4.0 ####
* pymybase.myloggingbase.MyLoggingBase added function (read|write)_file_key_values.  Switched to use these functions
* use folders.txt resource to handle where upload folder, sail input, and sail output folders are
* fixed a bug with MyTwitterAPI.  Proxy settings wasn't being used when generating a token.
* added all batch files so things can be scheduled easily
* little update to README.md

#### 2015-12-23 - 0.3.0 ####
* use proxy.txt to can work with any proxy setup
* copied new version of myjson2csv from pymybase project which now allows for column header to have an alias!
* retry on connection timeout
* added started sailtocsv.py which will process SAIL's output to the output/upload folder
* small bug fixes