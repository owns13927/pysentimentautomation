"""
@author: Elias Wood (owns13927@yahoo.com)
Created: 2015-12-14
A very basic Twitter API wrapper for getting twitter text 

CHANGELOG
2016-02-02 v0.1.5: Elias Wood
    changed default status query param trim_user to False
2015-12-14 v0.1.4: Elias Wood
    Got an error were it was waiting -1... changed to take
        abs(header new window time - int(time.time()))
"""

import requests
import logging
import time
from datetime import datetime
logging.getLogger('requests').setLevel(logging.WARNING)

from myloggingbase import MyLoggingBase
from myexceptions import TooManyRequests,MaxRetryLimit

class MyTwitterAPI(MyLoggingBase):
    __version__ = '0.1.5'
    #===========================================================================
    # Variables
    #===========================================================================
    _session = None
    __token = None
    __project = None
    _proxies = None
    
    #===========================================================================
    # Static Variables
    #===========================================================================
    URL_BASE = 'https://api.twitter.com'
    api_version = '1.1'
    # page size max is 5000 - go any higher and you'll get an error:
    # --- {u'errors': [{u'code': 0, u'message': u'Bad request: pageSize=9999 
    #                       expected int. must be less than or equal to 5000'}]}
    QUERY_PARAMS_STATUS = {'include_entities':False,'include_rts':False,
                           'trim_user':False,'map':True}
    DEFAULT_WAIT_DURATION = 900 # 15 minutes
    RETRY_CONNECTION_TIME = 60 # wait time if connection issue.
    WAIT_SALT = 15 # wait an extra X seconds for the window to open
    MAX_TRY_COUNT = 6 # make times to re-try a request
    
    #===========================================================================
    # Tracked Metrics
    #===========================================================================
    __requests_get_count = None
    __api_request_count = None
    __errors_returned = None
    __start_time = None
    __end_time = None
    
    def __init__(self,key,secret,*args,**keys):
        """ must pass application key and application secret to generate an
        application token for the app to use.
        """
        super(MyTwitterAPI, self).__init__(*args,**keys)
        
        # setup proxy
        self._proxies = dict()
        if 'proxies' in keys:
            if isinstance(keys['proxies'],dict):
                self.set_proxies(keys['proxies'])
            else: self.logger.warning('proxies must be a dictionary, e.g. http:http://user:pass@my.proxy.com:123')
        
        # generate the new token
        self.set_token(self.generate_app_token(key,secret))
        
        # start the connection session
        self.reset_session()
        
    #===========================================================================
    # Session
    #===========================================================================
    def reset_session(self,log_stats=False):
        """
        closes the current session (if one is already up)
        log_stats: will call log_summary_info(...)
                   before reseting the stats for the new session
        """
        if self.session_is_open():
            self.close_session()
            # log stats if asked
            if log_stats: self.log_summary_info()
        
        # open a new session
        self._session = requests.Session()
        self.__start_time = time.clock()
        self.__end_time = None
        if self.__token:
            self._session.headers['Authorization'] = 'Bearer ' + self.__token
        
        # set metrics start
        self.__requests_get_count = 0
        self.__api_request_count = 0
        self.__errors_returned = 0
        
    def session_is_open(self):
        """ determines if the session is open (not None) """
        return isinstance(self._session, requests.Session)
    
    def close_session(self):
        """closes session"""
        if self.session_is_open():
            self.__end_time = time.clock()
            self._session.close()
            self._session = None
    
    def close(self,*args,**keys):
        """closes session & anything else..."""
        self.close_session()
    
    #===========================================================================
    # Getters / Setters
    #===========================================================================
    def update_proxies(self,proxies):
        """updates the proxies used by the session"""
        self._proxies.update(proxies)
        
    def get_proxies(self):
        """returns a copy of the proxies being used by the session"""
        return self._proxies.copy()
    
    def set_proxies(self,proxies):
        """set the proxies used by the session"""
        self._proxies = proxies
        
    def set_token(self,token): self.__token = token
    #def get_token(self): return self.__token
        
    def set_api_version(self,v): self.api_version = v
    def get_api_version(self): return self.api_version
    
    def generate_app_token(self,token,secret):
        """ generates an application token
        instructions: https://dev.twitter.com/oauth/application-only 
        """
        
        from urllib import urlencode
        from base64 import b64encode
        
        r = requests.post(self.URL_BASE+'/oauth2/token',
                   data='grant_type=client_credentials',
                   headers={'Authorization':'Basic '+
                            b64encode(urlencode({token:secret}).replace('=',':')),
                    'Content-Type':'application/x-www-form-urlencoded;charset=UTF-8'
                },proxies=self._proxies)
        
        j = r.json()
        if r.status_code == 200:
            if j['token_type'] == 'bearer':
                self.logger.debug('new bearer token generated')
                return j['access_token']
            else:
                self.logger.critical('none bearer token... - %r',j)
                raise Exception('None "bearer" token_type - '+j['token_type'])
        else:
            self.logger.critical('failed - %r',j)
            raise Exception('Unknown error while trying to generate token.')
        
        return False

    #===========================================================================
    # Make an API call
    #===========================================================================
    def _api_parse(self,r):
        try: j = r.json()
        except ValueError as e: # TO JSON ERROR
            self.logger.warn('r.json() error %r',e)
            self.__errors_returned += 1
            raise e
        else:
            if 'errors' in j:           # API ERROR?
                #https://dev.twitter.com/rest/public/rate-limiting
                self.logger.warning('%r',j)
                self.__errors_returned += 1
                
                raise Exception(j['errors'][0]['message'])
            
        return j
    
    def _make_api_call(self,url,**params):
        # make the call
        self.__api_request_count += 1
        self.logger.debug('%03d:%s %r',self.__api_request_count,
                          url,params)
        
        try_count = 0
        while True:
            try_count += 1
            try:
                if try_count > self.MAX_TRY_COUNT: raise MaxRetryLimit(
                    'max retry limit of %s reached' % (self.MAX_TRY_COUNT,))
                
                extracted_dt = r = j = None
                
                self.__requests_get_count += 1
                r = self._session.get(url,params=params,proxies=self._proxies)
            
                # parse timestamp
                extracted_dt = self.datetime_to_timestamp(
                                        datetime.strptime(
                                        r.headers['date'],
                                        '%a, %d %b %Y %H:%M:%S %Z'))    
                
                # did we hit the rate limit 'Too Many Requests'?
                if r.status_code == 429:
                    raise TooManyRequests('HTTP 429: Too Many Requests')
                
                # error handling...
                #if r.status_code 1= 200: pass
                #else: pass
                
                # warning if last api called made...
                if r.headers.get('x-rate-limit-remaining') == '0':
                    self.logger.warning('0 / %s calls left at current API '+
                            'endpoint. %d seconds remaining in current window.',
                            r.headers['x-rate-limit-limit'],
                            int(r.headers['x-rate-limit-reset'])-time.time())
                
                # parse to json and handle errors
                j = self._api_parse(r)
                
            #=======================================================================
            # Error Handling
            #=======================================================================
            # if it's a connection issue, what a minute and try again.
            except requests.ConnectionError, e:
                #RETRY_CONNECTION_TIME
                self.logger.warning('ConnectionError - trying after %ds. %s',
                                    self.RETRY_CONNECTION_TIME,e)
                self.wait_to_retry(try_count,self.RETRY_CONNECTION_TIME)
            
            except (requests.RequestException,requests.Timeout,
                    requests.URLRequired,requests.TooManyRedirects,
                    requests.HTTPError) as e:
                self.logger.critical('requests error!!! %r',e)
                raise e # currently, just raise
            
            except TooManyRequests, e: 
                # wait for the necessary amount of time
                # https://dev.twitter.com/rest/public/rate-limiting
                #'x-rate-limit-limit': '60'
                #'x-rate-limit-reset': '1450289807'
                #'x-rate-limit-remaining': '59'
                
                t = int(r.headers['x-rate-limit-reset']) # desired time
                c = int(time.time()) # current time
                
                self.logger.warning('RATE LIMIT REACHED! '+
                            '0 / %s calls left at current API endpoint. '+
                            'Must wait %d+%d seconds for next window.',
                            r.headers['x-rate-limit-limit'],abs(t-c),self.WAIT_SALT)
                
                self.wait_to_retry(try_count,abs(t-c)+self.WAIT_SALT)
            
            except ValueError, e:
                # value error if r.json() fails in _api_parse(r)
                # wait some and start again - try again soon
                self.logger.warn('caught $r',e)
                self.wait_to_retry(try_count, self.WAIT_SALT)

            except Exception, e:
                self.logger.critical('error: %r',e)
                raise e
            
            else: break
            
        return extracted_dt, j
    
    def make_api_call(self,path,**params):
        """
        """
        # fix path if needed
        if path.startswith('/'): path = path[1:]
        
        # set url
        url = (path if path.startswith(self.URL_BASE) else
               '{}/{}/{}'.format(self.URL_BASE,self.api_version,path))
        
        # remove None params is exists
        if None in params.values():
            params = {k:v for k,v in params.iteritems() if v is not None}
        
        extracted_dt , j  = self._make_api_call(url,**params)
        
        #for d in ...: d['extracted_dt'] = extracted_dt
            
        return extracted_dt,j
        
    #===========================================================================
    # Functions to override!
    #===========================================================================
    def wait_to_retry(self,try_count,t=None):
        """ called whenever the crawler needs to wait, usually b/c
        a rate limit was reached.
        """
        if t is None: t = self.DEFAULT_WAIT_DURATION
        self.logger.info('waiting {} seconds - try {}'.format(t,try_count))
        self.before_sleeping(try_count)
        time.sleep(t)
        self.after_sleeping(try_count)
        
    def before_sleeping(self,try_count):
        """ called in self.wait_to_retry before sleeping """
        pass
    
    def after_sleeping(self,try_count):
        """ called in self.wait_to_retry after sleeping """
        pass

    #===========================================================================
    # Custom methods
    #===========================================================================
    def get_tweet_info(self,*tweetIds,**params):
        """info for tweets
        params updates the standard, self.QUERY_PARAMS_STATUS
        https://dev.twitter.com/rest/reference/get/statuses/lookup
        """
        #if len(tweetIds)==0:
        #   raise Exception('get_tweets_info: must have at least one tweet id!')
        
        d = self.QUERY_PARAMS_STATUS.copy()
        d.update(params)
        if tweetIds: d['id'] = ','.join(tweetIds)
        
        et,ids = self.make_api_call('statuses/lookup.json',**d); del d
        ids = ids['id']
        
        self.logger.info('%d / %d tweets extracted on %s',
                          len(ids),len(tweetIds),et)
        
        l = []
        for tweet in ids.itervalues(): #tweet_id,
            # can happen if unable to pull tweet (permissions, too old, etc)
            # see 'map' param
            if tweet is not None:
            #    l.append(dict(id=tweet_id,id_str=str(tweet_id),extracted_dt=et))
            #else:
                tweet['extracted_dt'] = et
                tweet['created_dt'] = self.standardize_twitter_date(
                                                        tweet['created_at'])
                if tweet.get('user',{}).get('created_at'):
                    tweet['user']['created_dt'] = self.standardize_twitter_date(
                                                        tweet['user']['created_at'])
                l.append(tweet)
        
        return l
    ''
    #===========================================================================
    # for parsing created_dt text...
    #===========================================================================
    @staticmethod
    def standardize_twitter_date(dt_str):
        """ twitter gives datetime created_dt in a non ISO 8601 standard.
        This static method does the conversion for you, 
        """
        return MyLoggingBase.datetime_to_timestamp(
                   datetime.strptime(dt_str,'%a %b %d %H:%M:%S +0000 %Y')) \
               if dt_str else None
    
    #===========================================================================
    # get into summary
    #===========================================================================
    def get_summary_info(self):
        """ add count of api calls and errors
        """
        a = super(MyTwitterAPI, self).get_summary_info()
        a.extend(('get requests: {:,}'.format(self.__requests_get_count),
                  'api requests: {:,}'.format(self.__api_request_count),
                  'error count: {:,}'.format(self.__errors_returned),
                  'session open: {:,} min'.format(((self.__end_time if self.__end_time
                                                    else time.clock()) - self.__start_time
                                                   )/60.0 if self.__start_time is not None
                                                  else -1)
                ))
        return a

''
#===============================================================================
# MAIN
#===============================================================================
if __name__ == '__main__':
    #try: from tests import test_fbapibase
    #except ImportError: print 'no test for fbapibase'
    #else: test_fbapibase.run_test()
    
    MyLoggingBase.init_logging(file_log_lvl=None)
    
    # get proxy info
    proxies = dict()
    d = MyLoggingBase.read_file_key_values(
        MyLoggingBase.get_resource_fd('proxy.txt')
    )
    if d.pop('enabled','1') == '1':
        proxies.update(d)
    
    # get twitter cred
    cred = MyLoggingBase.read_file_key_values(
                MyLoggingBase.get_resource_fd('twitter_cred.txt'),
                'key','secret')
        
    a = MyTwitterAPI(key=cred['key'],secret=cred['secret'],proxies=proxies)
    del proxies,cred
    
    # get example tweets
    with open(MyLoggingBase.get_resource_fd('zEx_twitter_ids.txt'),'r') as r:
        twitter_ids = tuple(i.rstrip() for i in r)
    
    print len(twitter_ids)
    
    from myjson2csv import MyJSON2CSV
    
    tw = MyJSON2CSV(filename=a.get_output_fd('tweets.txt'),name='tweets')
    tw.set_separator('\t')
    tw.set_qouting_all()
    tw.set_headers('id_str','text',
                   dict(key='text',key_fn=lambda key,value,default: \
                            value.encode('unicode-escape',),
                        name='easy view (uni esc)'),
                   dict(key='text',key_fn=lambda key,value,default: \
                            value.encode('ascii','ignore'),
                        name='to sail (ascii)'),
                   'lang','truncated','possibly_sensitive',
                   'extended_entities','favorite_count','retweet_count',
                   'coordinates','place','place.country_code','place.country',
                   'place.place_type','place.name','place.full_name','source',
                   'contributors','user.id_str','in_reply_to_screen_name',
                   'in_reply_to_user_id_str','in_reply_to_status_id_str',
                   'is_quote_status','quoted_status_id_str',
                   'retweeted_status.id','created_dt','extracted_dt')
    
    uw = MyJSON2CSV(filename=a.get_output_fd('twitter_user.txt'),name='users')
    uw.set_separator('\t')
    uw.set_qouting_all()
    uw.set_headers('id_str','name','screen_name','has_extended_profile',
                   'contributors_enabled','verified','protected',
                   'is_translator','lang','location','geo_enabled',
                   'default_profile_image','profile_image_url','description',
                   'url','entities','statuses_count','favourites_count',
                   'followers_count','friends_count','listed_count',
                   'utc_offset','time_zone','created_dt','extracted_dt')
    
    
    batch_size = 100
    for i in xrange(0,len(twitter_ids),batch_size):
        l = a.get_tweet_info(*twitter_ids[i:i+batch_size],trim_user=False)
        a.logger.info('%d results returned from %d to %d',len(d),i,i+batch_size)
        for tweet in l:
            tw.write_json_object(tweet)
            uw.write_json_object(dict(extracted_dt=tweet['extracted_dt'],
                                      **tweet.get('user',{})))
    
    a.close()
    a.log_summary_info()
    
    tw.close()
    tw.log_summary_info()
    
    uw.close()
    uw.log_summary_info()
    
    


