"""
@author: Elias Wood (owns13927@yahoo.com)
Created: 2015-12-09
go through Brandwatch queries and all twitter mentions, pull twitter text.
save to 2 separate csv files: 1 with brandwatch data, 1 with twitter data.
can be matched by        brandwatch.tweet_id = twitter.id_str
"""

import os
from shutil import move as shutil_move

from myloggingbase import MyLoggingBase
from myjson2csv import MyJSON2CSV
from brandwatchapi import BrandWatchAPI
from mytwitterapi import MyTwitterAPI

#===============================================================================
# Helper function for moving files
#===============================================================================
def move_keep_all_files(filepath,dest,logger=None):
    """ move a file to a destination without overwriting any files
    @param dest: the destination folder for the file
    @param filepath: the absolute file path
    @param logger: optional argument for logging if there is an error
    """
    filename = os.path.basename(filepath)
    filebase,fileext = os.path.splitext(filename)
    c = 0
    l = os.listdir(dest)
    
    # find a unique filename
    success = True
    while filename in l:
        c += 1
        filename = '{}_{}{}'.format(filebase,c,fileext) 
    
    newfilepath = os.path.join(dest,filename)
    
    # try to rename the file
    if logger: logger.debug('moving %s to %s',filepath,newfilepath)
    try: shutil_move(filepath, newfilepath)
    except Exception, e:
        if logger: logger.warning('error moving file %s to %s - %r',
                                  filepath,newfilepath,e) 
        success = False
    
    # how did we do?
    return success

#===============================================================================
# Helper function, creates sail writers
#===============================================================================
def create_sail_writer(name):
    # sail (view of twitter)
    w = MyJSON2CSV(filename=MyLoggingBase.get_output_fd(name+'.csv'),
                   name="J2C_"+name)
    w.name = name
    w.tweets = set()
    w.set_qouting_all()
    w.set_separator('\t')
    def to_tweet_text(key,value,default):
        value = value.encode('ascii','ignore')
        return (value + ' ') if value.endswith('\\') else value
    w.set_headers('queryId','queryName',
                  dict(key='id_str',name='url'), # tweet_id in aster
                  dict(key='user.id_str',name='user'), # user_id in aster
                  # SAIL needs to be baby-fed text...
                  dict(key='text',name="tweet_text",key_fn=to_tweet_text),
                  dict(key='created_dt',name='published_date'),'extracted_dt')
    return w

#===============================================================================
# MAIN
#===============================================================================
def main():
    m = MyLoggingBase(name='main')
    m.init_logging()#file_log_lvl=None)
    
    tweet_id_max = 100
    #query_start_date_delta = 9*60*60*24*30 # 1 month
    query_start_date = '1970-01-01' # epoch
    # if this file exists, we should stop running
    stop_filepath = m.get_output_fd('stop.txt')
    # this file exists means we're working!
    running_filepath = m.get_output_fd('running.txt')
    
    # don't run if running file exists!!!
    # the only issue is multiple instances may be querying for the same tweets
    # and they're using the same app ID, so they'll still be bottlenecked!
    if os.path.exists(running_filepath):
        m.logger.warn('already running. flag file exists! %s',running_filepath)
        return False
    
    with open(running_filepath,'w') as w:
        w.write('main.py is running :)')
    
    # setup csv exporting
    # brandwatch
    brandwatch_csv_filepath = m.get_output_fd('brandwatch.csv') 
    bw = MyJSON2CSV(filename=brandwatch_csv_filepath,
                   name="BW_JSON2CSV")
    bw.set_qouting_all()
    bw.set_separator('\t')
    bw.set_headers('queryId','queryName','resourceId','resourceType','tags',
                   'categoryDetails','continent','continentCode','country',
                   'countryCode','state','stateCode','county','countyCode',
                   'city','cityCode','latitude','longitude','author',
                   'authorContinent','authorContinentCode','authorCountry',
                   'authorCountryCode','authorState','authorStateCode',
                   'authorCounty','authorCountyCode','authorCity',
                   'authorCityCode','fullname','gender','avatarUrl',
                   'professions','interest','domain','pageType','accountType',
                   'url','mediaUrls','sentiment','fullText','language',
                   'wordCount','facebookAuthorId','facebookRole',
                   'facebookSubtype','facebookShares','facebookLikes',
                   'facebookComments','tweet_id','twitterAuthorId',
                   'twitterVerified','twitterRole','twitterFollowing',
                   'twitterFollowers','twitterRetweets','twitterPostCount',
                   'twitterReplyCount','forumViews','forumPosts','blogComments',
                   'status','retweetOf','replyTo','insightsHashtag',
                   'insightsMentioned','influence','averageDurationOfVisit',
                   'averageVisits','pagesPerVisit','monthlyVisitors',
                   'percentMaleVisitors','percentFemaleVisitors','engagement',
                   'importanceAmplification','importanceReach','impact',
                   'impressions','mozRank','outreach','reach','starred',
                   'trackedLinkClicks','trackedLinks','editorialValueEUR',
                   'editorialValueGBP','editorialValueUSD','assignment',
                   'lastAssignmentDate','backlinks','checked','date',
                   'displayUrls','expandedUrls','matchPositions','noteIds',
                   'priority','shortUrls','snippet','threadId','threadAuthor',
                   'threadEntryType','threadURL','threadCreated','extracted_dt')
    
    # twitter
    twitter_csv_filepath = m.get_output_fd('twitter.csv')
    tw = MyJSON2CSV(filename=twitter_csv_filepath,
                    name="TW_JSON2CSV")
    tw.set_qouting_all()
    tw.set_separator('\t')
    tw.set_headers(dict(key='id_str',name='tweet_id'),
                   dict(key='text',name='tweet_text'),
                   dict(key='text',name='text_uni_esc',key_fn=lambda \
                        key,value,default: value.encode('unicode_escape')),
                   'lang','truncated','possibly_sensitive','extended_entities',
                   'favorite_count','retweet_count','coordinates','place',
                   dict(key='place.country_code',name='country_code'),
                   dict(key='place.country',name='country'),
                   dict(key='place.place_type',name='place_type'),
                   dict(key='place.name',name='place_name'),
                   dict(key='place.full_name',name='place_full_name'),'source',
                   'contributors',dict(key='user.id_str',name='user_id'),
                   'in_reply_to_screen_name',
                   dict(key='in_reply_to_user_id_str',
                        name='in_reply_to_user_id'),
                   dict(key='in_reply_to_status_id_str',
                        name='in_reply_to_status_id'),
                   'is_quote_status',
                   dict(key='quoted_status_id_str',name='quoted_status_id'),
                   dict(key='retweeted_status.id',name='retweeted_status_id'),
                   'created_dt','extracted_dt')
    
    # sail (view of twitter)
    sws = dict()
    
    # setup mapping for finding which csv to put the twitter record in
    query_to_name = dict() # [queryId] --> config name
    p = MyLoggingBase.get_resource_fd('SAILconfigs')
    if not os.path.exists(p): os.mkdir(p)
    for ids in map(lambda t: {a:t[1] for a in t[0]},
                     ((d['queryIds'].split(','),d['name']) for d in \
                      (MyLoggingBase.read_file_key_values(os.path.join(p,f))
                       for f in os.listdir(p)) if d['queryIds'])):
        query_to_name.update(ids)
    
    # twitter users
    user_csv_filepath = m.get_output_fd('user.csv')
    uw = MyJSON2CSV(filename=user_csv_filepath,
                    name="UW_JSON2CSV")
    uw.set_qouting_all()
    uw.set_separator('\t')
    uw.set_headers(dict(key='id_str',name='user_id'),'name','screen_name',
                   'has_extended_profile','contributors_enabled','verified',
                   'protected','is_translator','lang','location','geo_enabled',
                   'default_profile_image','profile_image_url','description',
                   'url','entities','statuses_count','favourites_count',
                   'followers_count','friends_count','listed_count',
                   'utc_offset','time_zone','created_dt','extracted_dt')
    
    # get proxy info
    proxies = dict()
    d = m.read_file_key_values(m.get_resource_fd('proxy.txt'))
    if d.pop('enabled','1') == '1':
        proxies.update(d)
        m.logger.info('using proxies for %s',proxies.keys())
    
    # setup brandwatch api
    cred = m.read_file_key_values(m.get_resource_fd('brandwatch_cred.txt'),
                                  'token','project')
    bwapi = BrandWatchAPI(token=cred['token'],project=cred['project'],
                          proxies=proxies)
    del cred
    
    # setup twitter api
    cred = m.read_file_key_values(m.get_resource_fd('twitter_cred.txt'),
                                  'key','secret')
    twapi = MyTwitterAPI(key=cred['key'],secret=cred['secret'],proxies=proxies)
    del cred,proxies
    
    # remove stop file before we start
    #(why would you want to stop something before starting it?)
    if os.path.exists(stop_filepath):
        try: os.remove(stop_filepath)
        except Exception,e: m.logger.warn('del stop.txt failed at start, %r',e)
    
    # create queries resource folder if needed
    rs_queries_fd = m.get_resource_fd('queries/') 
    if not os.path.exists(rs_queries_fd): os.mkdir(rs_queries_fd)
    
    # get started - all the queries
    queries = bwapi.get_queries()
    
    failed = False
    for query in queries:
        m.logger.info('--------- %s: %s ----------',query['id'],query['name'])
        
        # get startDate for query - from file is exists
        startDate = m.read_file_key_values(
                        os.path.join(rs_queries_fd,str(query['id'])+'.txt'),
                        'startDate').get('startDate') or query_start_date
        
        # endDate a little in the past so we don't possible miss a tweet
        # we only get 1 page, which takes only a few seconds to pull.
        endDate = bwapi.get_current_timestamp(utc=True, add_sec=-3)
        try:
            for bw_tweet in bwapi.get_tweets_from_query(query['id'],
                                                        startDate=startDate,
                                                        endDate=endDate):
                bw.write_json_object(bw_tweet)
                
                # get which twitter writer we'll be using
                k = query_to_name.get(str(bw_tweet['queryId']),'default')
                if k in sws: cw = sws[k]
                else: cw = sws[k] = create_sail_writer(k)
                
                # add to get from twitter
                cw.tweets.add(bw_tweet['tweet_id'])
                
                # get twitter data if reached max
                if len(cw.tweets) == tweet_id_max:
                    #m.logger.info('getting %03d tweets...',len(tweet_ids))
                    for tw_tweet in twapi.get_tweet_info(*cw.tweets,
                                                         trim_user=False):
                        tw.write_json_object(tw_tweet)
                        cw.write_json_object(tw_tweet)
                        uw.write_json_object(dict(
                                    extracted_dt=tw_tweet['extracted_dt'],
                                    **tw_tweet.get('user',{})))
                    cw.tweets.clear()
                    
        except Exception, e:
            m.logger.critical('error caught - %r',e)
            failed = True
            break
        else:
            # save endDate to data file for query to use next time
            m.write_file_key_values(
                        m.get_resource_fd('queries/{}.txt'.format(query['id'])),
                        startDate=endDate,
                        queryName=query['name'].encode('UTF-8'))
            
        # stop if stop file exists
        if os.path.exists(stop_filepath):
            m.logger.info('stop file found - stopping...')
            try: os.remove(stop_filepath)
            except Exception,e: m.logger.warn('failed to delete stop file %r',e)
            break
    
    # for each sail input file
    for w in sws.itervalues():
        # get and save any twitter ids that may be left
        if not failed and len(w.tweets) != 0:
            m.logger.info("getting what's left - %03d tweets...",len(w.tweets))
            for tw_tweet in twapi.get_tweet_info(*w.tweets):
                tw.write_json_object(tw_tweet)
                w.write_json_object(tw_tweet)
            w.tweets.clear()
        # close the file propery and log details
        w.close()
        w.log_summary_info()
    
    # close brand watch api session
    bwapi.close()
    bwapi.log_summary_info()
    
    # close twitter api session
    twapi.close()
    twapi.log_summary_info()
    
    # close brand watch csv
    bw.close()
    bw.log_summary_info()
    brandwatch_csv_filepath = bw.get_filename()
    
    # close twitter csv
    tw.close()
    tw.log_summary_info()
    twitter_csv_filepath = tw.get_filename()

    # close user csv
    uw.close()
    uw.log_summary_info()
    user_csv_filepath = uw.get_filename()
    
    if failed: m.logger.info('failed - but still have good data!')
    
    # get output folders for csv files
    dest = m.read_file_key_values(m.get_resource_fd('folders.txt'),
                                  'uploadFolder','SAILInputFolder')
    
    # move files to  be processed by another application
    move_keep_all_files(brandwatch_csv_filepath,dest['uploadFolder'],m.logger)
    move_keep_all_files(twitter_csv_filepath,dest['uploadFolder'],m.logger)
    move_keep_all_files(user_csv_filepath,dest['uploadFolder'],m.logger)
    
    for w in sws.itervalues:
        fd = os.path.join(dest['SAILInputFolder'],w.name)
        if not os.path.exists(fd): os.mkdir(fd) # make dest folder if needed
        move_keep_all_files(w.filename,fd,m.logger) # move the file
    # delete running.txt file so other's know we're done
    try: os.remove(running_filepath)
    except IOError, e:
        m.logger.warning('error while trying to delete running file, %r',e)



if __name__ == '__main__':
    main()
    
    
    
    
    
