"""
Elias Wood (owns13927@yahoo.com)
2015-12-15
the exceptions used.... 
"""

class MyError(Exception):
    """ Base error """
    code = None
    def __init__(self,*args,**keys):
        self.code = keys.pop('code',None)
        Exception.__init__(self,*args,**keys)

class MaxRetryLimit(MyError):
    """ If re-try count for requests reached """

class TooManyRequests(MyError):
    """ HTTP error 429 - Too Many Requests """
    
class TwitterError(MyError):
    """ Error class to hold all twitter errors"""

class InvalidToken(MyError):
    """ raise if token has issues """
    
class InteruptError(MyError):
    """ raised if we are told to stop ASAP """